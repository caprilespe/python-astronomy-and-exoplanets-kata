# Description
This is a python 🐍 project kata 🥋 about how to show data of astronomy and exoplanets 🌎 🪐🔭

# Make with
[![Python](https://img.shields.io/badge/python-2b5b84?style=for-the-badge&logo=python&logoColor=white&labelColor=000000)]()